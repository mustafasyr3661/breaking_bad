const baseUrl ="https://www.breakingbadapi.com/api/";
const mainRoute='/';
const characterRoute='/character_screen';
const characterDetailsScreen='/character_details';
const seasonRoute='/season_screen';
const episodeRoute='/episode_screen';
const episodeInfo='/episode_info';
const List<String> seasonsPoster = [
  "https://static.wikia.nocookie.net/breakingbad/images/5/58/BB_S1_poster.jpg/revision/latest/top-crop/width/360/height/450",
  "https://liztellsfrank.files.wordpress.com/2019/01/a4b9de044d3967643e70a87827523ef2-e1546408675916.jpg",
  "https://i.imgur.com/KfxrcEe.jpg",
  "https://i.pinimg.com/originals/50/c9/47/50c947c65a9055a613a1c1967bb2ddf4.jpg",
  "https://hips.hearstapps.com/digitalspyuk.cdnds.net/12/23/bb.jpg?resize=480:*",
  ""
];