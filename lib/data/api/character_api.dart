import 'package:breaking_bad/conestans/strings.dart';
import 'package:dio/dio.dart';

class CharacterApi{
  late Dio dio;
  CharacterApi(){
    BaseOptions options= BaseOptions(
      baseUrl: baseUrl,
      receiveDataWhenStatusError: true,
      connectTimeout: 60*1000,
      receiveTimeout: 20*1000
    );
    dio=Dio(options);
  }
  Future <List<dynamic>> getAllCharacters ()async {
    try{
      Response response =await dio.get("characters");
      return response.data;
    }
    catch(error){
      print(error.toString());
      return [];
    }
  }

  Future <List<dynamic>> getQuotes (String whoSaidThisQuotes)async {
    try{
      Response response =await dio.get("quote", queryParameters : {"author":whoSaidThisQuotes});
      return response.data;
    }
    catch(error){
      print(error.toString());
      return [];
    }
  }

  Future <List<dynamic>> getAllEpisodes ()async {
    try{
      Response response =await dio.get("episodes");
      return response.data;
    }
    catch(error){
      print(error.toString());
      return [];
    }
  }

}