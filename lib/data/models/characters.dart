class Character
{
  late int charId;
  late String name;
  late String actorName;
  late String nickname;
  late String birthDay;
  late String image;
  late List<dynamic> jobs;
  late String statusIfDeadOrAlive;
  late List<dynamic>appearanceOfBreakingBadSeasons;
  late  String categoryForTwoSeries;
  late List<dynamic>appearanceOfBetterCallSaulSeasons;
  Character.fromJson(Map<String,dynamic>json){
    charId=json["char_id"];
    name=json["name"];
    nickname=json["nickname"];
    actorName=json["portrayed"];
    birthDay=json["birthday"];
    image=json["img"];
    jobs=json["occupation"];
    statusIfDeadOrAlive=json["status"];
    appearanceOfBreakingBadSeasons=json["appearance"];
    categoryForTwoSeries=json["category"];
    appearanceOfBetterCallSaulSeasons=json["better_call_saul_appearance"];

  }
}