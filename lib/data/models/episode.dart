class Episode {
  late int episodeId;
  late String episodeName;
  late String season;
  late String episodeNumber;
  late String series;
  late String dateOfEpisode;
  late List<dynamic> charNames;

  Episode.fromJson(Map<String, dynamic> json) {
    episodeId = json["episode_id"];
    episodeName = json["title"];
    season = json["season"];
    episodeNumber = json["episode"];
    series = json["series"];
    dateOfEpisode = json["air_date"];
    charNames = json["characters"];
  }
}
