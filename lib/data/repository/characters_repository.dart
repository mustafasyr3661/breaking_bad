import 'package:breaking_bad/data/api/character_api.dart';
import 'package:breaking_bad/data/models/characters.dart';
import 'package:breaking_bad/data/models/quote.dart';

class CharactersRepository{
  final CharacterApi characterApi;

  CharactersRepository(this.characterApi);
  Future <List<Character>> getAllCharacters()async{
    final characters =await characterApi.getAllCharacters();
    return characters.map((char) =>Character.fromJson(char)).toList();
  }

  Future <List<Quote>> getQuote(String whoSaidThisQuotes)async{
    final quotes=await characterApi.getQuotes(whoSaidThisQuotes);
    return quotes.map((aQuote) => Quote.fromJson(aQuote)).toList();
  }
}