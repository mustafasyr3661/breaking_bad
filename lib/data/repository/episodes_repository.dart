import 'package:breaking_bad/data/api/character_api.dart';
import 'package:breaking_bad/data/models/episode.dart';

class EpisodesRepository{
  final CharacterApi characterApi;

  EpisodesRepository(this.characterApi);

  Future <List<Episode>> getAllEpisodes()async{
    final episodes = await characterApi.getAllEpisodes();
    return episodes.map((episode)=>Episode.fromJson(episode)).toList();
  }

}