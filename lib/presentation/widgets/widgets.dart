import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';

Widget buildOfflineWidget() {
  return Center(
    child: Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Can't connect..check your network",
            style: TextStyle(
              fontSize: 22,
              color: MyColors.myGrey,
            ),
          ),
          Image.asset("assets/images/noInternet.png"),
        ],
      ),
    ),
  );
}
Widget buildOnlineWidget(Widget buildWidgets){
  return OfflineBuilder(
    connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
        ) {
      final bool connected = connectivity != ConnectivityResult.none;
      if (connected) {
        return buildWidgets;
      } else {
        return buildOfflineWidget();
      }
    },
    child: Center(
      child: CircularProgressIndicator(
        color: MyColors.myYellow,
      ),
    ),
  );
}