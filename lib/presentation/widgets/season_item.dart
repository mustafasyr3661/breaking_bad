import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/conestans/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SeasonItem extends StatelessWidget {
  final List<String> seasons;
  final int index;

  SeasonItem({
    Key? key,
    required this.seasons,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
      padding: EdgeInsetsDirectional.all(4),
      decoration: BoxDecoration(
        color: MyColors.myYellow,
        borderRadius: BorderRadius.circular(8),
      ),
      child: InkWell(
        onTap: () => Navigator.pushNamed(
          context,
          episodeRoute,
          arguments: index,
        ),
        child: GridTile(
          child: FadeInImage.assetNetwork(
            fit: BoxFit.cover,
            placeholder: 'assets/images/loading.gif',
            image: seasonsPoster[index],
          ),
          footer: Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            color: Colors.black54,
            alignment: Alignment.bottomCenter,
            child: Text(
              "Season: " + "${seasons[index]}",
              style: TextStyle(
                height: 1.3,
                fontSize: 16,
                color: MyColors.myWhite,
                fontWeight: FontWeight.bold,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
