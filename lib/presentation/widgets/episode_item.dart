import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/presentation/screens/episode_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EpisodeItem extends StatelessWidget {
  final String episodeTitle;
  final String episodeNumber;
  final int episodeId;

  const EpisodeItem(
      {Key? key, required this.episodeTitle, required this.episodeNumber,required this.episodeId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()=>showBottomSheet(context: context, builder: (context){
        return EpisodeInfoBuild(episodeId: episodeId,);
      }),
      child: Card(
        margin: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
        color: MyColors.myYellow.withOpacity(0.3),
        child: ListTile(
          title: Text(
            episodeTitle,
            style: TextStyle(
              fontSize: 20,
              color: MyColors.myWhite,
            ),
          ),
          leading: CircleAvatar(
            child: Text(
              episodeNumber,
              style: TextStyle(
                color: MyColors.myWhite,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: MyColors.myYellow,
          ),
        ),
      ),
    );
  }
}
