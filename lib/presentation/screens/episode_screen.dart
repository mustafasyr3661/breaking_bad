import 'package:breaking_bad/business_logic/episodes_cubit.dart';
import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/conestans/strings.dart';
import 'package:breaking_bad/data/models/episode.dart';
import 'package:breaking_bad/presentation/widgets/episode_item.dart';
import 'package:breaking_bad/presentation/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';

class EpisodeScreen extends StatelessWidget {
  final posterIndex;
  late List<Episode> allEpisode;
  List<String> allEpisodeTitle = [];
  List<String> allEpisodeNumbers = [];
  List<int> allEpisodeIds = [];

  EpisodeScreen({Key? key, required this.posterIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<EpisodesCubit>(context).getAllEpisodes();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.myYellow,
        leading: BackButton(
          color: MyColors.myGrey,
        ),
        title: Text(
          "Episode",
          style: TextStyle(color: MyColors.myGrey, fontSize: 18),
        ),
      ),
      body: buildOnlineWidget(buildBackGround()),
    );
  }

  Widget buildBackGround() {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(seasonsPoster[posterIndex]),
              fit: BoxFit.cover,
            ),
          ),
        ),
        BlocBuilder<EpisodesCubit, EpisodesState>(
          builder: (context, state) {
            if (state is EpisodesLoaded) {
              allEpisode = (state).episode;
              aEpisode();
              print("Iam here");
              return buildEpisodeList();
            } else {
              print("Iam not here");
              return buildShowIndicator();
            }
          },
        )
      ],
    );
  }

  Widget buildEpisodeList() {
    return ListView.separated(
      itemBuilder: (context, index) {
        return EpisodeItem(
          episodeTitle: allEpisodeTitle[index],
          episodeNumber: allEpisodeNumbers[index],
          episodeId: allEpisodeIds[index],
        );
      },
      separatorBuilder: (context, index) {
        return buildSeparator();
      },
      itemCount: allEpisodeTitle.length,
    );
  }

  Widget buildShowIndicator() {
    return Center(
      child: CircularProgressIndicator(
        color: MyColors.myYellow,
      ),
    );
  }

  Widget buildSeparator() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      color: MyColors.myGrey,
      height: 2,
    );
  }

  void aEpisode() {
    allEpisode.forEach((element) {
      if (element.season == (posterIndex + 1).toString() &&
          element.series == "Breaking Bad") {
        allEpisodeTitle.add(element.episodeName);
        allEpisodeTitle = allEpisodeTitle.toSet().toList();
        allEpisodeNumbers.add(element.episodeNumber);
        allEpisodeNumbers = allEpisodeNumbers.toSet().toList();
        allEpisodeIds.add(element.episodeId);
        allEpisodeIds = allEpisodeIds.toSet().toList();
      }
    });
  }
}
