import 'package:breaking_bad/business_logic/episodes_cubit.dart';
import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/data/models/episode.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EpisodeInfoBuild extends StatelessWidget {
  final int episodeId;
  List<Episode> episodeInfo = [];

  EpisodeInfoBuild({Key? key, required this.episodeId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: double.infinity,
      color: MyColors.myYellow.withOpacity(0.2),
      padding: EdgeInsetsDirectional.fromSTEB(10, 0, 10, 10),
      child: buildBottomSheet(),
    );
  }

  buildBottomSheet() {
    return BlocBuilder<EpisodesCubit, EpisodesState>(
      builder: (context, state) {
        if (state is EpisodesLoaded) {
          episodeInfo = (state).episode;
          print(episodeInfo.length);
          return buildInformation();
        } else {
          return buildCircleIndicator();
        }
      },
    );
  }

  Widget buildCircleIndicator() {
    return Center(
      child: CircularProgressIndicator(
        color: MyColors.myYellow,
      ),
    );
  }

  Widget buildInformation() {
    return Column(
      children: [
        Icon(Icons.arrow_drop_down_sharp,color: MyColors.myYellow,size: 25,),
        SizedBox(height: 30,),
        RichText(
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          text: TextSpan(
            children: [
              TextSpan(
                text: "shown on: ",
                style: TextStyle(
                  color: MyColors.myYellow,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
              TextSpan(
                text: "${episodeInfo[episodeId - 1].dateOfEpisode}",
                style: TextStyle(
                  color: MyColors.myGrey,
                  //fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10,),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Text(
                "Cast: ",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: MyColors.myYellow,
                ),
              ),
              Text(
                episodeInfo[episodeId - 1].charNames.join(' - '),
                style: TextStyle(
                  color: MyColors.myGrey,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
