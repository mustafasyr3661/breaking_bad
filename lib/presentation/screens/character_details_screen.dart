import 'dart:math';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:breaking_bad/business_logic/characters_cubit.dart';
import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/data/models/characters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CharactersDetailsScreen extends StatelessWidget {

  final Character character;
  const CharactersDetailsScreen({Key? key, required this.character})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
   CharactersCubit.get(context).getQuotes(character.name);
    return Scaffold(
      backgroundColor: MyColors.myGrey,
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          buildSliverList(),
        ],
      ),
    );
  }

  Widget buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 600,
      pinned: true,
      stretch: true,
      backgroundColor: MyColors.myGrey,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(
          character.nickname,
          style: TextStyle(
            color: MyColors.myWhite,
          ),
        ),
        centerTitle: true,
        background: Hero(
          tag: character.charId,
          child: Image.network(
            character.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget buildSliverList() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Container(
            margin: EdgeInsets.fromLTRB(14, 14, 14, 0),
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                characterInfo("Name : ", character.actorName),
                buildDivider(290),
                characterInfo("Berth day : ", character.birthDay),
                buildDivider(240),
                characterInfo("Job : ", character.jobs.join(' / ')),
                buildDivider(245),
                characterInfo("Appeared in : ", character.categoryForTwoSeries),
                buildDivider(250),
                characterInfo("Seasons : ",
                    character.appearanceOfBreakingBadSeasons.join(' / ')),
                buildDivider(280),
                characterInfo("Status : ", character.statusIfDeadOrAlive),
                buildDivider(300),
                character.appearanceOfBetterCallSaulSeasons.isEmpty
                    ? Container()
                    : characterInfo(
                        "Better Call Saul Seasons : ",
                        character.appearanceOfBetterCallSaulSeasons
                            .join(' / ')),
                character.appearanceOfBetterCallSaulSeasons.isEmpty
                    ? Container()
                    : buildDivider(290),
                SizedBox(
                  height: 20,
                ),
                BlocBuilder<CharactersCubit, CharactersState>(
                    builder: (context, state) {
                  return checkIfQuotesAreLoaded(state);
                }),
                SizedBox(
                  height: 400,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget characterInfo(String title, String value) {
    return RichText(
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        children: [
          TextSpan(
            text: title,
            style: TextStyle(
              color: MyColors.myWhite,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          TextSpan(
            text: value,
            style: TextStyle(
              color: MyColors.myWhite,
              //fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDivider(double endIndent) {
    return Divider(
      color: MyColors.myYellow,
      height: 30,
      endIndent: endIndent,
      thickness: 2,
    );
  }

  Widget checkIfQuotesAreLoaded(state) {
    if (state is QuoteLoaded) {
      print("i am at quotes state");
      return displayRandomQuoteOrEmptySpace(state);
    } else {
      print("i am not at quotes state");
      return showProgressIndicator();
    }
  }

  Widget displayRandomQuoteOrEmptySpace(QuoteLoaded state) {
    var quotes = (state).quotes;
    if (quotes.isNotEmpty) {
      int randomQuoteIndex = Random().nextInt(quotes.length - 1);
      return Center(
        child: DefaultTextStyle(
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: MyColors.myWhite,
            shadows: const [
              Shadow(
                color: MyColors.myYellow,
                blurRadius: 7,
                offset: Offset(0, 0),
              ),
            ],
          ),
          child: AnimatedTextKit(
            repeatForever: true,
            animatedTexts: [
              FlickerAnimatedText(
                quotes[randomQuoteIndex].quote,
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget showProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(
        color: MyColors.myYellow,
      ),
    );
  }

}
