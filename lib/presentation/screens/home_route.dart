import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/conestans/strings.dart';
import 'package:breaking_bad/presentation/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeRoute extends StatefulWidget {
  const HomeRoute({Key? key}) : super(key: key);

  @override
  State<HomeRoute> createState() => _HomeRouteState();
}

class _HomeRouteState extends State<HomeRoute> with TickerProviderStateMixin {
  late double _scale;
  late double _episodesScale;
  late AnimationController _controller;
  late AnimationController _episodesController;

  @override
  void initState() {
    _controller = AnimationController(
      duration: Duration(
        milliseconds: 500,
      ),
      vsync: this,
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    _episodesController = AnimationController(
      duration: Duration(
        milliseconds: 500,
      ),
      vsync: this,
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;
    _episodesScale = 1 - _episodesController.value;
    return Scaffold(
      backgroundColor: Colors.white,
      body:buildOnlineWidget(buildBackGround()),
    );
  }

  buildBackGround() {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/wallpaper.gif"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          child: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            title: Center(
                child: Text(
              "BreakingBad",
              style: TextStyle(
                color: MyColors.myWhite,
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),
            )),
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTapDown: _tapDown,
                onTapUp: _tapUp,
                child: Transform.scale(
                  scale: _scale,
                  child: _animatedButton("Characters"),
                ),
                onTap: () => Navigator.pushNamed(context, characterRoute),
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () => Navigator.pushNamed(context, seasonRoute),
                onTapDown: (TapDownDetails details) =>
                    _episodesController.forward(),
                onTapUp: (details) => _episodesController.reverse(),
                child: Transform.scale(
                  scale: _episodesScale,
                  child: _animatedButton("Episodes"),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _animatedButton(String title) {
    return Container(
      height: 70,
      width: 200,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          boxShadow: [
            BoxShadow(
              color: Color(0x80000000),
              blurRadius: 12.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.transparent,
              MyColors.myYellow,
            ],
          )),
      child: Center(
        child: Text(
          title,
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
    );
  }

  void _tapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _tapUp(TapUpDetails details) {
    _controller.reverse();
  }

}
