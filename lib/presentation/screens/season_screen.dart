import 'package:breaking_bad/business_logic/episodes_cubit.dart';
import 'package:breaking_bad/conestans/my_colors.dart';
import 'package:breaking_bad/data/models/episode.dart';
import 'package:breaking_bad/presentation/widgets/season_item.dart';
import 'package:breaking_bad/presentation/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SeasonsScreen extends StatefulWidget {
  const SeasonsScreen({Key? key}) : super(key: key);

  @override
  _SeasonsScreenState createState() => _SeasonsScreenState();
}

class _SeasonsScreenState extends State<SeasonsScreen> {
  late List<Episode> allEpisode;
  List<String> season = [];

  @override
  void initState() {
    BlocProvider.of<EpisodesCubit>(context).getAllEpisodes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.myGrey,
      appBar: AppBar(
        backgroundColor: MyColors.myYellow,
        title: Text(
          "Seasons",
          style: TextStyle(
            color: MyColors.myGrey,
            fontSize: 18,
          ),
        ),
        leading: BackButton(
          color: MyColors.myGrey,
        ),
      ),
      body: buildOnlineWidget(buildEpisodeWidget()),
    );
  }

  Widget buildEpisodeWidget() {
    return BlocBuilder<EpisodesCubit, EpisodesState>(builder: (context, state) {
      if (state is EpisodesLoaded) {
        print("Iam loaded");
        allEpisode = (state).episode;
        seasons();
        print(seasons());
        return buildEpisodeListWidget();
      } else {
        print("iam not loaded");
        return buildShowIndicator();
      }
    });
  }

  Widget buildEpisodeListWidget() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          buildSeasonsShow(),
        ],
      ),
    );
  }

  Widget buildShowIndicator() {
    return Center(
      child: CircularProgressIndicator(
        color: MyColors.myYellow,
      ),
    );
  }

  buildSeasonsShow() {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1,
            childAspectRatio: 2 / 3,
            crossAxisSpacing: 2,
            mainAxisSpacing: 1),
        shrinkWrap: true,
        physics: const ClampingScrollPhysics(),
        padding: EdgeInsets.zero,
        itemCount: seasons().length,
        itemBuilder: (context, index) {
          return SeasonItem(
            seasons: seasons(),
            index: index,
          );
        });
  }

  List<String> seasons() {
    allEpisode.forEach((element) {
      if (element.series == "Breaking Bad") {
        season.add(element.season);
      }
    });
    return season.toSet().toList().sublist(1).toList();
  }
}
