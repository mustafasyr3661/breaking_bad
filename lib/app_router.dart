import 'package:breaking_bad/business_logic/characters_cubit.dart';
import 'package:breaking_bad/conestans/strings.dart';
import 'package:breaking_bad/data/api/character_api.dart';
import 'package:breaking_bad/data/repository/characters_repository.dart';
import 'package:breaking_bad/data/repository/episodes_repository.dart';
import 'package:breaking_bad/presentation/screens/character_details_screen.dart';
import 'package:breaking_bad/presentation/screens/characters_screen.dart';
import 'package:breaking_bad/presentation/screens/episode_screen.dart';
import 'package:breaking_bad/presentation/screens/season_screen.dart';
import 'package:breaking_bad/presentation/screens/home_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'business_logic/episodes_cubit.dart';
import 'data/models/characters.dart';

class AppRouter {
  late CharactersRepository charactersRepository;
  late CharactersCubit charactersCubit;
  late EpisodesCubit episodesCubit;
  late EpisodesRepository episodesRepository;

  AppRouter() {
    charactersRepository = CharactersRepository(CharacterApi());
    charactersCubit = CharactersCubit(charactersRepository);
    episodesRepository = EpisodesRepository(CharacterApi());
    episodesCubit = EpisodesCubit(episodesRepository);
  }

  Route? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case mainRoute:
        return MaterialPageRoute(
          builder: (context) => HomeRoute(),
        );
      case characterRoute:
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => charactersCubit,
                  child: CharactersScreen(),
                ));
      case characterDetailsScreen:
        final character = settings.arguments as Character;
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => CharactersCubit(charactersRepository),
                  child: CharactersDetailsScreen(
                    character: character,
                  ),
                ));
      case seasonRoute:
        return MaterialPageRoute(
          builder: (Context) => BlocProvider(
            create: (context) => episodesCubit,
            child: SeasonsScreen(),
          ),
        );
      case episodeRoute:
        final posterIndex = settings.arguments as int;
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => EpisodesCubit(episodesRepository),
                  child: EpisodeScreen(posterIndex: posterIndex),
                ));

    }
  }
}
