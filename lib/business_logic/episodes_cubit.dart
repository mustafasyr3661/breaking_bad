import 'package:bloc/bloc.dart';
import 'package:breaking_bad/data/models/episode.dart';
import 'package:breaking_bad/data/models/episode_informaiton.dart';
import 'package:breaking_bad/data/repository/episodes_repository.dart';
import 'package:meta/meta.dart';

part 'episodes_state.dart';

class EpisodesCubit extends Cubit<EpisodesState> {
  EpisodesRepository episodesRepository;
  EpisodesCubit(this.episodesRepository) : super(EpisodesInitial());

  void getAllEpisodes(){
    episodesRepository.getAllEpisodes().then((value){
      emit(EpisodesLoaded(value));
    });
  }
}
