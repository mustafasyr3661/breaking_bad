import 'package:bloc/bloc.dart';
import 'package:breaking_bad/data/models/characters.dart';
import 'package:breaking_bad/data/models/quote.dart';
import 'package:breaking_bad/data/repository/characters_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'characters_state.dart';

class CharactersCubit extends Cubit<CharactersState> {
  static CharactersCubit get(context) => BlocProvider.of(context);

  final CharactersRepository charactersRepository;
   List<Character> characters=[];

  CharactersCubit(this.charactersRepository) : super(CharactersInitial());

  List<Character> getAllCharacters() {
    charactersRepository.getAllCharacters().then((value) {
      characters = value;
      emit(CharactersLoaded(value));
    });
    return characters;
  }

  void getQuotes(String whoSaidThisQuotes)
  {
    charactersRepository.getQuote(whoSaidThisQuotes).then((value) {
      emit(QuoteLoaded(value));
    });
  }


}
